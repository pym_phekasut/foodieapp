import classes from "./Card.module.css";

//need prop to access some children
const Card = (props) => {
  return (
    <div className={classes.card}>
      {props.children}
    </div>
  );
};

export default Card;
