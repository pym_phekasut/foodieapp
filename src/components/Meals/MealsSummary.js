import classes from "./MealsSummary.module.css";

const MealsSummary = () => {
  return (
    <section className={classes.summary}>
      <h2> Delicious Food, Delivered to you</h2>
      <p>
        Order from all of your favourite local and
        national restaurants or get groceries,
        snacks, essentials and alcohol delivered
        to your door.{" "}
      </p>
      <p>
        You choose your menu & we deliver top
        quality ingredients and simple recipe to
        your door. Quick and easy-to-follow
        recipes will make dinnertime absolutely
        delicious.
      </p>
    </section>
  );
};

export default MealsSummary;
